package com.hurnyakyaroslav.controller;

import com.hurnyakyaroslav.model.Model;
import com.hurnyakyaroslav.model.task4.Lines;

import java.util.Collections;
import java.util.LinkedList;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Controller {
    Model model = new Model();

    //Task1
    public Integer getMaxValue(int a, int b, int c) {
        return model.getMaxValue(a, b, c);
    }

    public Integer getAverageValue(int a, int b, int c) {
        return model.getAverageValue(a, b, c);
    }

    //Task2
    public String getCommands() {
        StringBuilder sb = new StringBuilder();
        model.getCommands().stream().forEach(e -> {
            sb.append(e.getString("My message"));
            sb.append("\n");
        });
        return sb.toString();
    }

    public String executeCommand(String name, String arg) {
        return model.executeCommand(name, arg);
    }

    //Task3
    public String generateArrays() {
        return (model.getTask3().toStream());
    }

    public String getStreamInfo() {
        Stream<Integer> stream = Stream.of(5, 4, 2, 5, 5, 7, 8, 2);
        return model.getTask3().getStreamInfo(stream).toString();
    }

    public String getMin() {
        Stream<Integer> stream = Stream.of(5, 4, 2, 5, 5, 7, 8, 2);
        return model.getTask3().getMin(stream).toString();
    }

    public String getMax() {
        Stream<Integer> stream = Stream.of(5, 4, 2, 5, 5, 7, 8, 2);
        return model.getTask3().getMax(stream).toString();
    }

    public String getSumm() {
        Stream<Integer> stream = Stream.of(5, 4, 2, 5, 5, 7, 8, 2);
        return model.getTask3().calculateSumm(stream).toString();
    }

    public String getAverage() {
        Stream<Integer> stream = Stream.of(5, 4, 2, 5, 5, 7, 8, 2);
        return model.getTask3().getAverage(stream).toString();
    }

    public String getLarge() {
        Stream<Integer> stream = Stream.of(5, 4, 2, 5, 5, 7, 8, 2);
        return model.getTask3().getLargeCount(stream).toString();
    }


    //Task4
    Lines lines;

    public void initLines(String str) {
        lines = new Lines(str);
    }

    public Lines getLines() {
        return lines;
    }
}
