package com.hurnyakyaroslav.view;

import com.hurnyakyaroslav.controller.Controller;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

public class ConsoleView {
    private Controller controller;
    private static Logger logger;

    public ConsoleView() {
        controller = new Controller();
        logger = LogManager.getLogger(ConsoleView.class);
    }

    public void show() {
        //task1
        logger.trace(controller.getMaxValue(4, 2, 7));

        logger.trace(controller.getAverageValue(4, 2, 7));

        //task2

        logger.trace(controller.executeCommand("lambda", "myMessage"));
//        logger.trace(controller.getCommands());

        logger.trace(controller.generateArrays());
//

//
        logger.trace(controller.getMax());
        logger.trace(controller.getMin());
        logger.trace(controller.getAverage());
        logger.trace(controller.getSumm());
        logger.trace(controller.getLarge());

        Scanner scanner = new Scanner(System.in);
        StringBuilder lines = new StringBuilder();
        String tmp;
        do {
            lines.append("\n");
            tmp = scanner.nextLine();
            lines.append(tmp);
        } while (!tmp.equals(""));

        controller.initLines(lines.toString());

        logger.trace(controller.getLines().getWordStatistic());

    }
}
