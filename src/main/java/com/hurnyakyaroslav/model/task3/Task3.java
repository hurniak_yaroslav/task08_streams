package com.hurnyakyaroslav.model.task3;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Task3 {
    /**
     * Create a few methods that returns list (or array) of random integers. Methods should use streams API and
     * should be implemented using different Streams generators.
     * Count average, min, max, sum of list values. Try to count sum using both reduce and sum Stream methods
     * Count number of values that are bigger than average
     */
    List<Integer> integerList = new ArrayList<>();

    private void initList() {
        for (int i = 0; i < 20; i++) {
            integerList.add(new Integer((int) (Math.random() * 100)));
        }
    }

    public String toStream() {

        Stream<Integer> stream = Stream.generate(() -> new Integer((int) (Math.random() * 100))).limit(20);
        Stream<Integer> stream1 = Stream.iterate(2, t -> (t + 5)).limit(20);
        Stream<Integer> stream2 = Stream.of(5, 6, 3, 9);

        StringBuilder sb;
        sb = new StringBuilder();
        sb.append("\n");
        stream.forEach(e -> {
            sb.append(e.intValue() + " ");
        });
        sb.append("\n");
        stream1.forEach(e -> {
            sb.append(e.intValue() + " ");
        });
        sb.append("\n");
        stream2.forEach(e -> {
            sb.append(e.intValue() + " ");
        });
        sb.append("\n");
        return sb.toString();
    }

    public Map<String, Optional<Integer>> getStreamInfo(Stream<Integer> stream) {
        Map<String, Optional<Integer>> map = new HashMap<>();
        map.put("min", stream.min(Integer::compareTo));
        map.put("max", stream.max(Integer::compareTo));
        map.put("average", Optional.of(calculateSumm(stream) / (int) (stream.count())));
        map.put("sum", Optional.of(new Integer(calculateSumm(stream))));
        map.put("large", Optional.of((int) stream.filter((s) -> (s > map.get("average").get())).count()));
        return map;
    }

    public Integer getMax(Stream<Integer> stream) {
        return (stream.max(Integer::compareTo).get());
    }

    public Integer getMin(Stream<Integer> stream) {
        return (stream.min(Integer::compareTo).get());
    }

    public Integer getAverage(Stream<Integer> stream) {
        ArrayList<Integer> streamArray = stream.collect(Collectors.toCollection(ArrayList::new));
        int summ = streamArray.stream().reduce((sum, elem) -> (sum += elem)).get();

        return (summ / (int)(streamArray.stream().count()));
    }

    public Integer getLargeCount(Stream<Integer> stream) {
        ArrayList<Integer> streamArray = stream.collect(Collectors.toCollection(ArrayList::new));
        return (int) streamArray.stream().filter((s) -> (s > this.getAverage(streamArray.stream()))).count();
    }


    public Integer calculateSumm(Stream<Integer> stream) {

        Integer summ = stream.reduce((sum, elem) -> (sum += elem)).get();
        return summ;
    }

}
