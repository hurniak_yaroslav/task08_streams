package com.hurnyakyaroslav.model;

import com.hurnyakyaroslav.model.task1.ThreeFunctional;
import com.hurnyakyaroslav.model.task2.Command;
import com.hurnyakyaroslav.model.task2.CommandImp;
import com.hurnyakyaroslav.model.task3.Task3;
import com.hurnyakyaroslav.model.task4.Lines;

import java.util.LinkedList;
import java.util.List;

public class Model {

    //Task1

    /**
     * Create functional interface with method that accepts three int values and return int value.
     * Create lambda functions (as variables in main method) what implements this interface:
     * First lambda returns max value
     * Second – average
     */
    public int getMaxValue(int a, int b, int c) {
        ThreeFunctional threeFunctional = (a1, a2, a3) -> {
            if (a1 > a2 && a1 > a3) return a1;
            else if (a2 > a1 && a2 > a3) return a2;
            else return a3;
        };
        return threeFunctional.calculate(a, b, c);
    }

    public int getAverageValue(int a, int b, int c) {
        ThreeFunctional threeFunctional = (a1, a2, a3) -> ((a1 + a2 + a3) / 3);
        return threeFunctional.calculate(a, b, c);
    }

    /**
     * Implement pattern Command. Each command has its name (with which it is invoked) and one string argument.
     * You should implement 4 commands with next ways: command as lambda function, as method reference, as anonymous
     * class, as object of command class. User enters command name and argument into console,
     * your app invokes corresponding command.
     */

    List<Command> commands = new LinkedList<>();

    private void initList() {
        commands.add(new Command() {
            @Override
            public String getString(String str) {
                return "Called as anononymous class.";
            }
        });

        commands.add((str) -> ("Called as lambda. Your message: " + str));
        commands.add(this::getString);
        commands.add(new CommandImp());


    }

    public List<Command> getCommands() {
        commands.clear();
        initList();
        return commands;
    }

    private String getString(String s) {
        return "Called as method reference. Your message: " + s;
    }

    public String executeCommand(String name, String arg) {
        String result = null;
        try {
            commands.clear();
            initList();
            if (name.equals("anonymous")) {
                result = commands.get(0).getString(arg);
            } else if (name.equals("lambda")) {
                result = commands.get(1).getString(arg);
            } else if (name.equals("reference")) {
                result = commands.get(2).getString(arg);
            } else if (name.equals("object")) {
                result = commands.get(3).getString(arg);
            }
        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace();
        }
        return result;
    }


    Task3 task3 = new Task3();


    public Task3 getTask3() {
        return task3;
    }

}