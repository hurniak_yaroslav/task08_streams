package com.hurnyakyaroslav.model.task1;

@FunctionalInterface
public interface ThreeFunctional {
    int calculate(int a, int b, int c);
}
