package com.hurnyakyaroslav.model.task4;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;


/**
 * Create application. User enters some number of text lines (stop reading text when user enters empty line). Application returns:
 * Number of unique words
 * Sorted list of all unique words
 * Word count. Occurrence number of each word in the text (e.g. text “a s a” -> a-2 s-1 ). Use grouping collector.
 * Occurrence number of each symbol except upper case characters
 */
public class Lines {
    String lines;
    Stream<String> stringStream;

    public Lines(String lines) {
        this.lines = lines;
        stringStream = Stream.of(lines);
    }

    public int getUniqueWordsCount() {
        return (int) stringStream.distinct().count();
    }

    public List<String> getSortedUniqueWords() {
        return stringStream.distinct().collect(Collectors.toList());
    }

    public Map<String, Long> getWordStatistic() {
        return stringStream.flatMap(e -> Stream.of(e.split(" "))).collect(Collectors.groupingBy(e -> e, Collectors.counting()));
    }

    public Map<String, Long> getSymbolStatistic() {
        return stringStream.flatMap(e -> Stream.of(e.split(""))).collect(Collectors.groupingBy(e -> e, Collectors.counting()));
    }


}
