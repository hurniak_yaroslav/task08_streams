package com.hurnyakyaroslav.model.task2;

public class CommandImp implements Command{

    @Override
    public String getString(String str) {
        return "Called as object of command class.";
    }
}
