package com.hurnyakyaroslav.model.task2;

@FunctionalInterface
public interface Command {
    String getString(String str);
}
